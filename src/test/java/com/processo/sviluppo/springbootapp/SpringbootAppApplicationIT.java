package com.processo.sviluppo.springbootapp;

import com.processo.sviluppo.springbootapp.model.Message;
import com.processo.sviluppo.springbootapp.service.MainService;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * La classe SpringbootAppApplicationIT si occupa del test di integrazione.
 * @author L.L.T., M.C.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootAppApplicationIT {

    /**
     * Viene iniettato il MainService che serve per testare l'interazione del backend con il database.
     */
    @Autowired
    MainService mainService;

    /**
     * Il test e' molto semplice e consiste nel postare un messaggio sul database con testo "Prova22" e recuperarlo grazie al suo id (id = 1
     * perchè ad ogni esecuzione del programma il database h2 si rigenera vuoto), controllando che il testo del messaggio recuperato
     * sia uguale effettivamente a "Prova22")
     */
    @Test
    public void testDiIntegrazione() {

        String textMessage = "Questo è un integration test";
        mainService.addMessage(textMessage);

        Message found = mainService.findById(1).get();
        assertEquals(found.getText(), textMessage);

    }

}
