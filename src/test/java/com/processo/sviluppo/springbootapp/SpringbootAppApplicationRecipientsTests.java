package com.processo.sviluppo.springbootapp;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.processo.sviluppo.springbootapp.model.Recipient;
import com.processo.sviluppo.springbootapp.repository.RecipientRepository;
import com.processo.sviluppo.springbootapp.service.RecipientService;

/**
 * La classe SpringbootAppApplicationTestsRecipients si occupa del test di unita' per le classi legate all'entità Recipient.
 * @author L.L.T., M.C.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootAppApplicationRecipientsTests {
    
    /**
     * Viene iniettato il RecipientRepository;
     */
    @Mock
    RecipientRepository repository;
    
    /**
     * Viene iniettato il RecipientService.
     */
    @InjectMocks
    RecipientService recipientService;
    
    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Il test consiste 
     */
    @Test
    public void testUnita() {

        List<Recipient> list = new ArrayList<Recipient>();
        Recipient recOne = new Recipient(1, "John");
        Recipient recTwo = new Recipient(2, "Alex");
        Recipient recThree = new Recipient(3, "Steve");
         
        list.add(recOne);
        list.add(recTwo);
        list.add(recThree);
         
        when(repository.findAll()).thenReturn(list);
         
        //test
        List<Recipient> recList = recipientService.getAllRecipients();
         
        assertEquals(3, recList.size());
        verify(repository, times(1)).findAll();
        
    }

}
