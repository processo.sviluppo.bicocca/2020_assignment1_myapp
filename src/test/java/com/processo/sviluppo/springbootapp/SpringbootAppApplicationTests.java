package com.processo.sviluppo.springbootapp;

import static org.junit.Assert.assertEquals;

import com.processo.sviluppo.springbootapp.model.Message;
import com.processo.sviluppo.springbootapp.service.MainService;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * La classe SpringbootAppApplicationTests si occupa del test di unita' per le classi legate all'entità Message.
 * @author L.L.T., M.C.
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootAppApplicationTests {

    /**
     * Viene iniettato il MainService perche' contiene il metodo con cui si ordina alfabeticamente una lista di messaggi. Non vengono
     * usati i metodi che interagiscono con il database, per questo c'e' l'integration test.
     */
    @Autowired
    MainService mainService;

    /**
     * Il test consiste nel creare due liste di messaggi che contengono gli stessi elementi, una ordinata in ordine alfabetico e l'altra no.
     * Si passa la lista non ordinata nel metodo sort del MainService e poi si confrontano le liste, controllando che quella passata dal 
     * metodo sort sia ora uguale a quella già ordinata.
     */
    @Test
    public void testUnita() {

        List<Message> msgs = new ArrayList<>();

        msgs.add(new Message("Alto"));
        msgs.add(new Message("Basso"));
        msgs.add(new Message("Corto"));

        /////////////////////////////////////////

        List<Message> msgs2 = new ArrayList<>();

        msgs2.add(new Message("Alto"));
        msgs2.add(new Message("Corto"));
        msgs2.add(new Message("Basso"));

        msgs2 = mainService.sort(msgs2);

        for(int i=0;i<3;i++) {

            assertEquals( msgs.get(i).getText() , msgs2.get(i).getText() );

        }

    }

}
