package com.processo.sviluppo.springbootapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringbootAppApplication e' la classe che serve ad eseguire la web app springboot-app,
 * parte dell'assignment 1 del corso di Processo e Sviluppo del Software.
 * @author L.L.T., M.C.
 *
 */
@SpringBootApplication
public class SpringbootAppApplication {

    /**
     * Esegue l'applicazione come una SpringBootApplication
     * @param args
     */
    public static void main(final String[] args) {

        SpringApplication.run(SpringbootAppApplication.class, args);

    }

}
