package com.processo.sviluppo.springbootapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.processo.sviluppo.springbootapp.model.Message;
import com.processo.sviluppo.springbootapp.model.Recipient;
import com.processo.sviluppo.springbootapp.service.MainService;
import com.processo.sviluppo.springbootapp.service.RecipientService;

/**
 * La classe MainController contiene il controller che esegue i metodi GET che servono a postare o mostrare i messaggi caricati sul
 * database.
 * @author L.L.T., M.C.
 *
 */
@RestController
public class MainController {

    /**
     * Viene iniettato nel MainController il MainService, il servizio che si occupa di chiamare il repository per salvare o
     * recuperare i messaggi dal database.
     */
    @Autowired
    MainService mainService;

    /**
     * Viene iniettato nel MainController il RecipientService, il servizio che si occupa di chiamare il repository per salvare o
     * recuperare i destinatari dal database.
     */
    @Autowired
    RecipientService recipientService;
    
    /**
     * Ritorna una lista di oggetti json che rappresentano tutti i messaggi presenti sul database, ordinati per id.
     * @return messages
     */
    @GetMapping("/messages")
    public List<Message> getAllMessages() {		

        return mainService.getAllMessages();

    }

    /**
     * Posta un messaggio sul database con id autogenerato e testo scritto nell'URI: /messages/{messageText}.
     * @param messageText
     * @return a check number
     */
    @GetMapping("/messages/{messageText}")
    public int addMessage(@PathVariable String messageText) {

        mainService.addMessage(messageText);
        return 1;

    }

    /**
     * Ritorna il messaggio con id uguale a quello scritto nell'URI: /message/{messageId}.
     * @param messageId
     * @return message
     */
    @GetMapping("/message/{messageId}")
    public Message getMessage(@PathVariable Integer messageId) {

        return mainService.findById(messageId).get();

    }

    /**
     * Ritorna una lista di oggetti json che rappresentano tutti i messaggi presenti sul database, ordinati alfabeticamente per testo.
     * @return messages
     */
    @GetMapping("/messagesOrder")
    public List<Message> getAllMessagesOrder() {

        List<Message> msgs = mainService.getAllMessages();
        return mainService.sort(msgs);

    }
    
    @GetMapping("/recipients")
    public List<Recipient> getAllRecipients() {
        
        return recipientService.getAllRecipients();
        
    }
    
    @GetMapping("/recipients/{messageId}/{name}")
    public void addRecipient(@PathVariable Integer messageId, @PathVariable String name) {
        
        recipientService.addRecipient(messageId, name);
        
    }

}
