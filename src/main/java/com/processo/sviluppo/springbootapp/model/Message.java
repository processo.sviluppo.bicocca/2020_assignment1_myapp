package com.processo.sviluppo.springbootapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * La classe Message e' l'entita' che rappresenta il messaggio, composto da un codice id che lo identifica univocamente dagli altri
 * messaggi e un testo.
 * @author L.L.T., M.C.
 *
 */
@Entity
public class Message {

    /**
     * Codice identificativo del messaggio.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * Testo del messaggio.
     */
    private String text;

    /**
     * Costruttore di default.
     */
    public Message() { }

    /**
     * Costruttore che prende in ingresso il testo del messaggio. L'id viene autogenerato in fase di salvataggio sul database.
     * @param text
     */
    public Message(String text) {

        this.text = text;

    }

    /**
     * Getter per il parametro id.
     * @return id
     */
    public Integer getId() {

        return id;

    }

    /**
     * Setter per il parametro id.
     * @param id
     */
    public void setId(Integer id) {

        this.id = id;

    }

    /**
     * Getter per il parametro text.
     * @return text
     */
    public String getText() {

        return text;

    }

    /**
     * Setter per il parametro text.
     * @param text
     */
    public void setText(String text) {

        this.text = text;

    }

}
