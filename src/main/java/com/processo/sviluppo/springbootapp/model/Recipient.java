package com.processo.sviluppo.springbootapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * La classe Recipient e' l'entita' che rappresenta il destinatario di un messaggio, composto da un codice id che lo identifica univocamente dagli altri
 * destinatari, un nome e il messaggio di cui è recipient.
 * @author L.L.T., M.C.
 *
 */
@Entity
public class Recipient {
    
    /**
     * Codice identificativo del destinatario.
     */
    @Id
    @GeneratedValue
    private Integer id;

    /**
     * Nome del destinatario.
     */
    private String name;
    
    /**
     * Messaggio di cui è destinatario.
     */
    private Integer messageId;
    
    /**
     * Costruttore di default.
     */
    public Recipient() { }

    /**
     * Costruttore che prende in ingresso il nome del destinatario e il messaggio a cui è riferito. L'id viene autogenerato in fase di salvataggio sul database.
     * @param text
     */
    public Recipient(Integer messageId, String name) {

        this.name = name;
        this.messageId = messageId;
        
    }

    /**
     * Getter per il parametro id
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Setter per il parametro id
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Getter per il parametro name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter per il parametro name
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter per il parametro message
     * @return message
     */
    public Integer getMessageId() {
        return messageId;
    }

    /**
     * Setter per il parametro message
     * @param message
     */
    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }
    
    

}
