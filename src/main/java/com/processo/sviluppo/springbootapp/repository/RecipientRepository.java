package com.processo.sviluppo.springbootapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.processo.sviluppo.springbootapp.model.Message;
import com.processo.sviluppo.springbootapp.model.Recipient;

/**
 * La classe RecipientRepository si occupa di salvare e recuperare le entità destinatario dal database.
 * @author L.L.T., M.C.
 *
 */
@Repository
public interface RecipientRepository extends CrudRepository<Recipient, Integer> {

}
