package com.processo.sviluppo.springbootapp.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.processo.sviluppo.springbootapp.model.Message;

/**
 * La classe MessageRepository si occupa di salvare e recuperare le entità messaggio dal database.
 * @author L.L.T., M.C.
 *
 */
@Repository
public interface MessageRepository extends CrudRepository<Message, Integer> {

}
