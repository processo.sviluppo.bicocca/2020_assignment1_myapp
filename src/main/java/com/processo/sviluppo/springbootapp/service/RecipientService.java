package com.processo.sviluppo.springbootapp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.processo.sviluppo.springbootapp.model.Recipient;
import com.processo.sviluppo.springbootapp.repository.RecipientRepository;

/**
 * La classe RecipientService si occupa di preparare i destinatari per chiamare il RecipientRepository, allo scopo di salvare o 
 * recuperare i destinatari salvati sul database.
 * @author L.L.T., M.C.
 *
 */
@Service
public class RecipientService {
    
    /**
     * Viene iniettato nel MainService il RecipientRepository, il repository dei destinatari che si occupa del dialogo vero e proprio con il database.
     */
    @Autowired
    RecipientRepository recipientRepo;
    
    /**
     * Ritorna una lista di oggetti json che rappresentano tutti i destinatari presenti sul database, ordinati per id.
     * @return messages
     */
    public List<Recipient> getAllRecipients() {

        List<Recipient> recipients = new ArrayList<Recipient>();
        recipientRepo.findAll().forEach(recipient -> recipients.add(recipient));
        return recipients;

    }
    
    /**
     * Aggiunge un destinatario database con id autogenerato, nome e id messaggio scritti nell'URI: /recipients/{messageId}/{name}.
     * @param messageId
     * @param name
     */
    public void addRecipient(Integer messageId, String name) {
        
        recipientRepo.save(new Recipient(messageId, name));
        
    }
    
}
