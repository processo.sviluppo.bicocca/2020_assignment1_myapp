package com.processo.sviluppo.springbootapp.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.processo.sviluppo.springbootapp.model.Message;
import com.processo.sviluppo.springbootapp.repository.MessageRepository;

/**
 * La classe MainService si occupa di preparare le liste e i messaggi per chiamare il MessageRepository, allo scopo di salvare o 
 * recuperare i messaggi salvati sul database.
 * @author L.L.T., M.C.
 *
 */
@Service
public class MainService {

    /**
     * Viene iniettato nel MainService il MessageRepository, il repository dei messaggi che si occupa del dialogo vero e proprio con il database.
     */
    @Autowired
    MessageRepository messageRepo;
    
    /**
     * Ritorna una lista di oggetti json che rappresentano tutti i messaggi presenti sul database, ordinati per id.
     * @return messages
     */
    public List<Message> getAllMessages() {

        List<Message> messages = new ArrayList<Message>();
        messageRepo.findAll().forEach(message -> messages.add(message));
        return messages;

    }

    /**
     * Posta un messaggio sul database con id autogenerato e testo scritto nell'URI: /messages/{messageText}.
     * @param newMessageText
     * @return a check number
     */
    public int addMessage(String newMessageText) {

        messageRepo.save(new Message(newMessageText));
        return 1;

    }

    /**
     * Ritorna il messaggio con id uguale a quello scritto nell'URI: /message/{messageId}.
     * @param id
     * @return message
     */
    public Optional<Message> findById(Integer id) {

        return messageRepo.findById(id);

    }

    /**
     * Ordina la lista di messaggi in input in ordine alfabetico rispetto al parametro text.
     * @param msgs
     * @return messages
     */
    public List<Message> sort(List<Message> msgs){
        msgs.sort(new Comparator<Message>() {

            public int compare(Message a, Message b) {

                return a.getText().charAt(0) - b.getText().charAt(0);

            }

        });

        return msgs;

    }

}
