Corso Processo e Sviluppo del Software. Assignment 1

URL REPO: https://gitlab.com/processo.sviluppo.bicocca/2020_assignment1_myapp

Questo Assignment numero 1 è stato fatto dal gruppo: Tonelli Lidia Lucrezia (813114), Casola Massimo (875411) in una collaborazione continua ma, volendo a fine didattici distiguere il lavoro, possiamo assegnare:

	Tonelli Lidia Lucrezia : Sviluppo Java con framework web, Sviluppo Unit Test(anche con Mock), documentazione
	Casola Massimo :  Sviluppo Pipeline & plugin Java di verifica sul codice, Sviluppo Integration Test,Deploy

APPLICAZIONE
	MyApp è un applicazione web Java che sfrutta il framework Spring Boot per lo sviluppo di un backend, con cui salva e recupera dei "messaggi" e dei "destinatari" da un database H2.
	L'applicazione è composta da 6 metodi GET:
		- Andando sul route "/messages" si ottiene una lista di oggetti json che rappresentano i messaggi salvati sul database.
		- Andando sul route "/messages/{messageText}" si salva un nuovo messaggio sul database.
		- Andando sul route "/message/{messageId}" si ottiene il messaggio corrispondente all'id.
		- Andando sul route "/messagesOrder" si ottiene la lista di messaggi ordinata per ordine alfabetico.
		- Andando sul route "/recipients" si ottiene la lista dei destinatari.
		- Andando sul route "/recipients/{messageId}/{name}" si salva un nuovo destinatario riferito al messaggio con id messageId e con nome name.

TEST
	I test creati per questa pipeline sono 3, due unit test e un integration test:
		lo unit test dell'unità dei messaggi testa il buon funzionamento del metodo che ordina alfabeticamente i messaggi
		lo unit test dell'unità dei destinatari testa, con dei mock che sostituiscono il repository e il service, il buon funzionamento del salvataggio di destinatari sul database
		l'integration test posta un messaggio sul database e poi lo recupera tramite id (id=1 perchè per ogni esecuzione del programma il database si azzera), confrontando che il testo sia uguale a quello del messaggio salvato

PIPELINE
	La pipeline invece è stata sviluppata utilizzando lo strumento di continuous integration "pipelines" di GitLab ed il registry interno sempre di GitLab.
	
	Essendo un progetto Java, sfrutta maven per le fasi di build, verify, unit-test, integration-test, package.
	Dopodichè nella fase di release dockerizza il progetto e fa il push dell'immagine docker su un registry interno di Gitlab
	
	La pipeline è composta dalle fasi:
		- build: compila l'app SpringBoot con Maven.
		- verify: fase composta da due stage eseguiti in parallelo, si occupa di controllare la correttezza della sintassi e dello stile del codice
			- lint: check style. Questo stage fallisce, però abbiamo impostato il flag allow_failure a true dato che fa controlli molto
				stretti sul codice, ad esempio controlla che esistano i JavaDoc (che comunque abbiamo scritto), la correttezza dei tab e degli
				spazi; questo controllo non è necessario per il buon funzionamento dell'app.
			- sats: compie analisi statiche del codice con SpotBugs.
		- unit test: fase composta da due stage eseguiti in parallelo
			- unit-test-messages: compie lo unit test sulle classi legate all'entità Message, ovvero il service e il repository dei messaggi
			- unit-test-recipients: compie lo unit test sulle classi legate all'entità Recipient, ovvero il service e il repository dei destinatari
		- integration test: esegue l'integration test, si esegue solo sul branch master dato che testa il funzionamento di più componenti dell'app assieme.
		- package: impacchetta l'app SpringBoot creando il file .jar.
		- release: costruisce l'immagine Docker dell'app e la carica nel Container Registry di GitLab; usa il Dockerfile nel repository root.
		- deploy: per il deploy abbiamo scelto il provider Azure, sfruttando il suo servizio di App Service; questo servizio identifica 
			eventuali modifiche del codice attraverso l'URL del repository, effettuando, in ottica di Continuous Delivery, rilasci automatici sul sito
			https://apppsb.azurewebsites.net
			Nella pipeline è stata implementata una fase di deploy che effettua la stampa in console di un reminder che mostra il link su cui si trova l'app,
			rilasciata in CD da Azure, inoltre viene stampato anche lo status code della risposta del sito, ottenuto con il comando curl.
		
	Durante l'esecuzione della pipeline si fa uso di variabili condivise e di una cache che viene splittata per alcuni job: la cache è definita per tutta la pipeline
	e ridefinita per alcuni job come il lint, il sats, i test e la fase di package. 
	La cache è disabilitata per la fase di release, poichè viene usato il caching di Docker.
